/*global Promise*/
// file-storage.js

import {
  promises,
  existsSync,
  mkdirSync,
  createWriteStream,
  createReadStream,
} from 'fs';
import { dirname as _dirname } from 'path';
import os from 'os';
const fsPromises = promises;

import { FILE_EXTENSION_REGEX } from '../config.mjs';

class FileStorageService {
  constructor(options = {}) {
    this._logger = options.logger ?? console;
    this._folder = undefined;
  }

  set logger(val) {
    this._logger = val;
  }

  get logger() {
    return this._logger;
  }

  set folder(val) {
    this._folder = val;
  }

  get folder() {
    return this._folder;
  }

  validateBase64String(base64String) {
    if (
      base64String === undefined ||
      typeof base64String !== 'string' ||
      !base64String.trim().startsWith('data:')
    ) {
      throw new Error('Invalid Base64 String');
    }
  }

  extractMetadataFromBase64String(base64String) {
    this.validateBase64String(base64String);
    return base64String.substring(0, base64String.indexOf(','));
  }

  extractPayloadFromBase64String(base64String) {
    this.validateBase64String(base64String);
    return base64String.substring(base64String.indexOf(',') + 1);
  }

  extractMimeTypeFromBase64String(base64String) {
    const metadata = this.extractMetadataFromBase64String(base64String);
    return metadata.substring(0, metadata.indexOf(';')).split(/:/)[1];
  }

  getFileExtensionForBase64String(base64String) {
    return this.getFileExtension(
      this.extractMimeTypeFromBase64String(base64String)
    );
  }

  getFileExtension(mimeType) {
    const metadata = this.getFileMetadata(mimeType);
    return metadata ? metadata.fileExtension : '';
  }

  getFileExtensionMatches(mimeType) {
    const metadata = this.getFileMetadata(mimeType);
    return metadata ? metadata.matches : [];
  }

  getFileMetadata(mimeType) {
    return this.getFilesMetadata()[mimeType];
  }

  async saveBase64PayloadToFile({ filePath = '', payload = '' }) {
    const data = Buffer.from(payload, 'base64');
    return await this.saveFile(filePath, data);
  }

  async saveFile(filePath = '', data = '') {
    try {
      const dirname = _dirname(filePath);

      if (!existsSync(dirname)) {
        await fsPromises.mkdir(dirname, { recursive: true });
      }

      await fsPromises.writeFile(filePath, data);
    } catch (error) {
      this._logger.error(error);
      return false;
    }

    return true;
  }

  async removeFile(filePath = '') {
    try {
      if (existsSync(filePath)) {
        await fsPromises.rm(filePath);
      }
    } catch (error) {
      this._logger.error(error);
      return false;
    }

    return true;
  }

  async createReadStream(filePath) {
    return createReadStream(filePath);
  }

  async readFile(filePath) {
    return await fsPromises.readFile(filePath);
  }

  async saveStreamToFile(filePath = '', readStream = null) {
    return new Promise((resolve, reject) => {
      try {
        if (!readStream) {
          return reject(false);
        }

        readStream.setEncoding('binary');

        const dirname = _dirname(filePath);

        if (!existsSync(dirname)) {
          mkdirSync(dirname, { recursive: true });
        }

        const writeStream = createWriteStream(filePath);
        writeStream.setDefaultEncoding('binary');

        readStream
          .pipe(writeStream)
          .on('finish', () => {
            this._logger.info(`Data successfully written to ${filePath}`);
            writeStream.end();
            resolve(true);
          })
          .on('error', (error) => {
            this._logger.error(error);
            reject(false);
          });
      } catch (error) {
        this._logger.error(error);
        return reject(false);
      }
    });
  }

  async getFileWebUrl({ baseUrl = '', urlPath = '' }) {
    return new URL(urlPath, baseUrl);
  }

  hasFileExtension({ name = '', mimeType = '' }) {
    if (name === undefined || typeof name !== 'string') {
      return false;
    }

    const fileExtensionMatch = name.match(FILE_EXTENSION_REGEX);

    if (!fileExtensionMatch) {
      return false;
    }

    const fileExtensionMatches = this.getFileExtensionMatches(mimeType);

    return fileExtensionMatches.includes(fileExtensionMatch[0]);
  }

  removeFileExtension(name) {
    if (name === undefined || typeof name !== 'string') {
      return '';
    }

    const fileExtensionMatch = name.match(FILE_EXTENSION_REGEX);

    if (!fileExtensionMatch) {
      return name;
    }

    return name.substring(0, fileExtensionMatch['index']);
  }

  getFilesMetadata() {
    return {
      'image/png': {
        mimeType: 'image/png',
        fileExtension: '.png',
        matches: ['.png'],
      },
      'image/jpeg': {
        mimeType: 'image/jpeg',
        fileExtension: '.jpg',
        matches: ['.jpe', '.jpg', '.jpeg', '.jfif', '.pjpeg', '.pip'],
      },
      'image/svg+xml': {
        mimeType: 'image/svg+xml',
        fileExtension: '.svg',
        matches: ['.svg'],
      },
      'image/gif': {
        mimeType: 'image/gif',
        fileExtension: '.gif',
        matches: ['.gif'],
      },
      'image/tiff': {
        mimeType: 'image/gif',
        fileExtension: '.tiff',
        matches: ['.tif', '.tiff'],
      },
      'video/x-msvideo': {
        mimeType: 'video/x-msvideo',
        fileExtension: '.avi',
        matches: ['.avi'],
      },
      'video/quicktime': {
        mimeType: 'video/quicktime',
        fileExtension: '.mov',
        matches: ['.mov'],
      },
      'video/x-sgi-movie': {
        mimeType: 'video/x-sgi-movie',
        fileExtension: '.movie',
        matches: ['.movie'],
      },
      'video/mp4': {
        mimeType: 'video/mp4',
        fileExtension: '.mp4',
        matches: ['.mp4'],
      },
      'video/mpeg': {
        mimeType: 'video/mp4',
        fileExtension: '.mpg',
        matches: ['.mpe', '.mpeg', '.mpg'],
      },
      'video/vnd.mpegurl': {
        mimeType: 'video/vnd.mpegurl',
        fileExtension: '.m4u',
        matches: ['.m4u'],
      },
      'audio/x-mpegurl': {
        mimeType: 'audio/x-mpegurl',
        fileExtension: '.m3u',
        matches: ['.m3u'],
      },
      'audio/mp4a-latm': {
        mimeType: 'audio/mp4a-latm',
        fileExtension: '.m4u',
        matches: ['.m4u'],
      },
      'audio/midi': {
        mimeType: 'audio/midi',
        fileExtension: '.midi',
        matches: ['.mid', '.midi'],
      },
      'audio/mpeg': {
        mimeType: 'audio/mpeg',
        fileExtension: '.mp3',
        matches: ['.mp2', '.mp3', '.mpga'],
      },
      'audio/x-wav': {
        mimeType: 'audio/x-wav',
        fileExtension: '.wav',
        matches: ['.wav'],
      },
      'application/msword': {
        mimeType: 'application/msword',
        fileExtension: '.doc',
        matches: ['.doc'],
      },
      'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
        {
          mimeType:
            'application/vnd.openxmlformats-officedocument.wordprocessingml.document',
          fileExtension: '.docx',
          matches: ['.docx'],
        },
      'application/oda': {
        mimeType: 'application/oda',
        fileExtension: '.oda',
        matches: ['.oda'],
      },
      'application/ogg': {
        mimeType: 'application/ogg',
        fileExtension: '.ogg',
        matches: ['.ogg', 'ogx'],
      },
      'application/pdf': {
        mimeType: 'application/pdf',
        fileExtension: '.pdf',
        matches: ['.pdf'],
      },
      'application/vnd.ms-powerpoint': {
        mimeType: 'application/vnd.ms-powerpoint',
        fileExtension: '.ppt',
        matches: ['.ppt'],
      },
      'application/vnd.ms-excel': {
        mimeType: 'application/vnd.ms-excel',
        fileExtension: '.xls',
        matches: ['.xls'],
      },
      'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet': {
        mimeType:
          'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        fileExtension: '.xlsx',
        matches: ['.xlsx'],
      },
      'application/epub+zip': {
        mimeType: 'application/epub+zip',
        fileExtension: '.epub',
        matches: ['.epub'],
      },
      'text/csv': {
        mimeType: 'text/csv',
        fileExtension: '.csv',
        matches: ['.csv'],
      },
      'application/vnd.oasis.opendocument.presentation': {
        mimeType: 'application/vnd.oasis.opendocument.presentation',
        fileExtension: '.odp',
        matches: ['.odp'],
      },
      'application/vnd.oasis.opendocument.spreadsheet': {
        mimeType: 'application/vnd.oasis.opendocument.spreadsheet',
        fileExtension: '.ods',
        matches: ['.ods'],
      },
      'application/vnd.oasis.opendocument.text': {
        mimeType: 'application/vnd.oasis.opendocument.text',
        fileExtension: '.ods',
        matches: ['.ods'],
      },
      'audio/ogg': {
        mimeType: 'audio/ogg',
        fileExtension: '.oga',
        matches: ['.oga'],
      },
      'video/ogg': {
        mimeType: 'video/ogg',
        fileExtension: '.ogv',
        matches: ['.ogv'],
      },
      'audio/wav': {
        mimeType: 'audio/wav',
        fileExtension: '.wav',
        matches: ['.wav'],
      },
      'audio/weba': {
        mimeType: 'audio/weba',
        fileExtension: '.weba',
        matches: ['.weba'],
      },
      'audio/webm': {
        mimeType: 'audio/webm',
        fileExtension: '.webm',
        matches: ['.webm'],
      },
      'image/webp': {
        mimeType: 'image/webp',
        fileExtension: '.webp',
        matches: ['.webp'],
      },
      'application/zip': {
        mimeType: 'application/zip',
        fileExtension: '.zip',
        matches: ['.zip'],
      },
    };
  }

  getTmpdir() {
    return os.tmpdir();
  }
}

export default FileStorageService;
