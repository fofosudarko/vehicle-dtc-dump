// index.js

import { parse } from 'csv-parse/sync';
import { MongoClient } from 'mongodb';

import CsvService from './lib/csv.mjs';
import FileStorageService from './lib/file-storage.mjs';

import { createRequire } from 'module';
const require = createRequire(import.meta.url);
const { codes: dtcCodes } = require('./codes.json');

const csvFiles = [
  { name: 'b-codes', path: './b-codes.csv' },
  { name: 'u-codes', path: './u-codes.csv' },
  { name: 'p-codes', path: './p-codes.csv' },
  { name: 'c-codes', path: './c-codes.csv' },
];

const csvService = new CsvService({ parseFn: parse });
const fileStorageService = new FileStorageService();

// iterate through csv files
// read each csv file and parse the content to the csv service
// generate an array using the csv service

const csvOptions = {
  columns: true,
  skip_empty_lines: true,
  quote: null,
  ignore_last_delimiters: true,
};

let data = [];

for (const csvFile of csvFiles) {
  console.log(`Parsing csv for ${csvFile.name}...`);
  const csvData = await fileStorageService.readFile(csvFile.path);
  data = [
    ...data,
    ...(await csvService.parseSync(csvData, csvOptions)).map((entry) => ({
      ...entry,
      createdOn: new Date(),
    })),
  ];
}

const dtcCodesList = [];

for (const key of Object.keys(dtcCodes)) {
  dtcCodesList.push({ code: key, description: dtcCodes[key] });
}

const dtcCodesListCodes = new Set(dtcCodesList.map((i) => i.code));
const dataCodes = new Set(data.map((i) => i.code));
const codesDifference = [...dtcCodesListCodes].filter((i) => !dataCodes.has(i));

data = [
  ...data,
  ...dtcCodesList
    .filter((i) => codesDifference.includes(i.code))
    .map((i) => ({ ...i, createdOn: new Date() })),
];

console.log('Data: ', data);

function constructOptions() {
  const queryParams = new URLSearchParams('');
  if (
    process.env.MONGODB_SERVER_TLS !== undefined &&
    process.env.MONGODB_SERVER_TLS !== ''
  ) {
    queryParams.set('tls', process.env.MONGODB_SERVER_TLS);
  }
  if (
    process.env.MONGODB_SERVER_TLS_CA_FILE !== undefined &&
    process.env.MONGODB_SERVER_TLS_CA_FILE !== ''
  ) {
    queryParams.set('tlsCAFile', process.env.MONGODB_SERVER_TLS_CA_FILE);
  }
  if (
    process.env.MONGODB_SERVER_RETRY_WRITES !== undefined &&
    process.env.MONGODB_SERVER_RETRY_WRITES !== ''
  ) {
    queryParams.set('retryWrites', process.env.MONGODB_SERVER_RETRY_WRITES);
  }
  return '?' + queryParams.toString();
}

// Connection URI
const uri = `mongodb://${process.env.MONGODB_SERVER_USERNAME}:${
  process.env.MONGODB_SERVER_PASSWORD
}@${process.env.MONGODB_SERVER_HOST}:${
  process.env.MONGODB_SERVER_PORT
}/${constructOptions()}`;

const client = new MongoClient(uri);

async function run(data) {
  try {
    await client.connect();

    const database = client.db(process.env.MONGODB_SERVER_DATABASE);
    const vehicleDiagnosticTroubleCodes = database.collection(
      'VehicleDiagnosticTroubleCodes'
    );
    const count = await vehicleDiagnosticTroubleCodes.countDocuments();
    if (count === 0) {
      const result = await vehicleDiagnosticTroubleCodes.insertMany(data, {
        ordered: true,
      });
      console.log(`Documents inserted: ${result.insertedCount}`);
    } else {
      console.log(`${count} documents inserted already`);
    }
  } finally {
    await client.close();
  }
}

run(data).catch(console.dir);
